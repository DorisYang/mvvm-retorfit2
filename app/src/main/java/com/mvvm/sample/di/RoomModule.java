package com.mvvm.sample.di;

import com.mvvm.sample.AppExecutors;
import com.mvvm.sample.BasicApp;
import com.mvvm.sample.db.AppDatabase;
import com.mvvm.sample.db.BoxDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RoomModule {
    @Singleton
    @Provides
    AppDatabase providesRoomDatabase(BasicApp mApplication) {
        return AppDatabase.getInstance(mApplication);
    }

    @Singleton
    @Provides
    BoxDao providesFoodDao(AppDatabase appDatabase) {
        return appDatabase.boxDao();
    }

}
