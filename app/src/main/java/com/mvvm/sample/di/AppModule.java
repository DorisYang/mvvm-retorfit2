package com.mvvm.sample.di;

import com.mvvm.sample.api.UserService;
import com.mvvm.sample.api.RetrofitGenerator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {RoomModule.class})
public class AppModule {

    @Singleton
    @Provides
    UserService provideLoginService(){
        return RetrofitGenerator.createService(UserService.class);
    }
}
