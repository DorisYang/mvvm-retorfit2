package com.mvvm.sample.di;

import com.mvvm.sample.ui.check.CheckActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class BuildersModule {

    @ContributesAndroidInjector
    abstract CheckActivity contributeCheckActivity();

}
