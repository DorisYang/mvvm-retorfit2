package com.mvvm.sample.di;

import com.mvvm.sample.BasicApp;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {AndroidInjectionModule.class, AppModule.class, BuildersModule.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(BasicApp application);
        AppComponent build();
    }

    void inject(BasicApp mainApp);
}