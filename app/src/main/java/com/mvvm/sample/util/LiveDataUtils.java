package com.mvvm.sample.util;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;

import java.util.ArrayList;

public class LiveDataUtils {
    public static LiveData<ArrayList<Object>> zipLiveData(LiveData<Object>... liveItems){
        final ArrayList<Object> zippedObjects = new ArrayList<>();
        final MediatorLiveData<ArrayList<Object>> mediator = new MediatorLiveData<>();
        for(LiveData<Object> item: liveItems){
            mediator.addSource(item, object ->{
                if(!zippedObjects.contains(object)){
                    zippedObjects.add(object);
                }
                mediator.setValue(zippedObjects);
            });
        }
        return mediator;
    }
}
