package com.mvvm.sample.util;

import android.app.Dialog;
import android.content.Context;

import com.mvvm.sample.R;
import com.mvvm.sample.ui.base.CustomDialog;

public class DialogFactory {
    public static Dialog createLoadingDialog(Context context) {
        Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_loading);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);
        return dialog;
    }

    public static CustomDialog createCustomDialog(Context context) {
        CustomDialog customDialog = new CustomDialog(context);
        return customDialog;
    }
}
