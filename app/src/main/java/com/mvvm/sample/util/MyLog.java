package com.mvvm.sample.util;

import com.mvvm.sample.BuildConfig;

public class MyLog {
    public static final String APP_TAG = "FainBox";
    public static boolean debugMode = BuildConfig.DEBUG;

    public static void debug(Object... arr) {
        // in java, Thread.currentThread().getStackTrace()[2];
        StackTraceElement call = Thread.currentThread().getStackTrace()[3];
        String className = call.getClassName();
        className = className.substring(className.lastIndexOf('.') + 1);
        if (debugMode)
            android.util.Log.v(APP_TAG, call.getLineNumber() + ": " + className + "." + call.getMethodName() + " " + java.util.Arrays.deepToString(arr));
    }

    public static void error(Object... arr) {
        // in java, Thread.currentThread().getStackTrace()[2];
        StackTraceElement call = Thread.currentThread().getStackTrace()[3];
        String className = call.getClassName();
        className = className.substring(className.lastIndexOf('.') + 1);
        if (debugMode)
            android.util.Log.e(APP_TAG, call.getLineNumber() + ": " + className + "." + call.getMethodName() + " " + java.util.Arrays.deepToString(arr));
    }
}
