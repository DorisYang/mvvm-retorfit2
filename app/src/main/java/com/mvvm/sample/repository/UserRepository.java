package com.mvvm.sample.repository;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.mvvm.sample.api.ApiResponse;
import com.mvvm.sample.api.UserService;
import com.mvvm.sample.db.BoxDao;
import com.mvvm.sample.util.AbsentLiveData;
import com.mvvm.sample.util.MyLog;
import com.mvvm.sample.vo.BoxInfo;
import com.mvvm.sample.vo.ErrorCode;
import com.mvvm.sample.vo.Resource;
import com.mvvm.sample.vo.response.CheckMacAddressResponse;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import okhttp3.ResponseBody;

public class UserRepository extends BaseRepository{
    private final UserService mUserService;
    private final BoxDao mBoxDao;

    @Inject
    public UserRepository(UserService userService, BoxDao boxDao) {
        mUserService = userService;
        mBoxDao = boxDao;
    }

    public LiveData<Resource<BoxInfo>> checkMacAddress(String macAddress) {
        return new NetworkBoundResource<BoxInfo, CheckMacAddressResponse>() {
            @Override
            protected void saveCallResult(@NonNull CheckMacAddressResponse item) {
                mBoxDao.insertBoxBoundStatus(item.data);
            }

            @NonNull
            @Override
            protected LiveData<BoxInfo> loadFromDb() {
                return AbsentLiveData.create();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<CheckMacAddressResponse>> createCall() {
                Map<String, String> mac = new HashMap<>();
                mac.put("MACaddress", macAddress);
                return mUserService.checkMACAddress(mac);
            }

            @Override
            protected void showLoading() {
                showLoadingDialog();
            }

            @Override
            protected void closeLoading() {
                closeLoadingDialog();
            }
        }.getAsLiveData();
    }

    public LiveData<ApiResponse<ResponseBody>> connectToGoogle(){
        return mUserService.connectToGoogle();
    }


}
