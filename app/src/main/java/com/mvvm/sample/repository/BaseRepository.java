package com.mvvm.sample.repository;

import android.arch.lifecycle.MutableLiveData;

import com.mvvm.sample.util.SingleLiveEvent;
import com.mvvm.sample.vo.ErrorCode;

public class BaseRepository {
    private SingleLiveEvent<Boolean> isShow = new SingleLiveEvent<>();
    private MutableLiveData<ErrorCode> mErrorCode = new MutableLiveData<>();

    public SingleLiveEvent<Boolean> getShowStatus(){
        return isShow;
    }

    public void showLoadingDialog(){
        isShow.setValue(true);
    }

    public void closeLoadingDialog(){
        isShow.setValue(false);
    }

    public MutableLiveData<ErrorCode>  getErrorMesage(){
        return mErrorCode;
    }

    public void setErrorMesage(ErrorCode errorCode){
        mErrorCode.setValue(errorCode);
    }
}
