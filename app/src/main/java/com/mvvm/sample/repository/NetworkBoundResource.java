package com.mvvm.sample.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.os.AsyncTask;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;

import com.mvvm.sample.api.ApiResponse;
import com.mvvm.sample.vo.HttpStatus;
import com.mvvm.sample.vo.Resource;


public abstract class NetworkBoundResource<ResultType, RequestType> {
    private final MediatorLiveData<Resource<ResultType>> result = new MediatorLiveData<>();

    @MainThread
    NetworkBoundResource() {
        result.setValue(Resource.<ResultType>loading(null));
        LiveData<ResultType> dbSource = loadFromDb();
        result.addSource(dbSource, data -> {
            result.removeSource(dbSource);
            if (shouldFetch(data)) {
                fetchFromNetwork(dbSource);
            } else {
                result.addSource(dbSource, newData -> result.setValue(Resource.success(newData)));
            }
        });

    }

    private void fetchFromNetwork(final LiveData<ResultType> dbSource) {
        try {
            if (dbSource.getValue() == null)
                showLoading();
            LiveData<ApiResponse<RequestType>> apiResponse = createCall();
            result.addSource(dbSource, newData -> result.setValue(Resource.loading(newData)));
            result.addSource(apiResponse, response -> {
                result.removeSource(apiResponse);
                result.removeSource(dbSource);
                //noinspection ConstantConditions
                closeLoading();
                if (response.isSuccessful()) {
                    HttpStatus status = (HttpStatus) response.body;
                    if (status.status.code == 200) {
                        saveResultAndReInit(response);
                    } else {
                        if (status != null && status.errorCode != null) {
                            result.addSource(dbSource,
                                    newData -> result.setValue(
                                            Resource.error(status.errorCode.code,status.errorCode.errorMsg, newData)));
                        } else {
                            result.addSource(dbSource,
                                    newData -> result.setValue(
                                            Resource.error("-1","JSON null", newData)));
                        }
                    }
                } else {
                    result.addSource(dbSource,
                            newData -> result.setValue(
                                    Resource.error(String.valueOf(response.code),response.errorMessage, newData)));
                }

            });
        } catch (Exception e) {
            result.addSource(dbSource,
                    newData -> result.setValue(
                            Resource.error("-1",e.getMessage(), newData)));

        }
    }

    @MainThread
    private void saveResultAndReInit(ApiResponse<RequestType> response) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                saveCallResult(response.body);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                LiveData<ResultType> dbSource = loadFromDb();
                result.addSource(dbSource, newData -> {
                    result.removeSource(dbSource);
                    result.setValue(Resource.success(newData));
                });
            }
        }.execute();
    }

    @WorkerThread
    protected abstract void saveCallResult(@NonNull RequestType item);

    @MainThread
    protected boolean shouldFetch(@Nullable ResultType data) {
        return true;
    }

    protected void showLoading() {

    }

    protected void closeLoading() {

    }


    @NonNull
    @MainThread
    protected abstract LiveData<ResultType> loadFromDb();

    @NonNull
    @MainThread
    protected abstract LiveData<ApiResponse<RequestType>> createCall();

    @MainThread
    protected void onFetchFailed() {
    }

    public final LiveData<Resource<ResultType>> getAsLiveData() {
        return result;
    }
}
