package com.mvvm.sample.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.mvvm.sample.vo.BoxInfo;

@Dao
public abstract class BoxDao {
    @Query("select * from BoxInfo")
    public abstract LiveData<BoxInfo> getBox();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertBoxBoundStatus(BoxInfo isBound);

}
