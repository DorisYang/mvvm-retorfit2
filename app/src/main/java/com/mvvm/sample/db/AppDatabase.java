package com.mvvm.sample.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;

import com.mvvm.sample.vo.BoxInfo;

@Database(entities = {BoxInfo.class}, version = 1, exportSchema = false)
public abstract class AppDatabase  extends RoomDatabase {
    private static AppDatabase INSTANCE;

    public abstract BoxDao boxDao();

    private static final Object sLock = new Object();

    public static AppDatabase getInstance(final Context context) {
        synchronized (sLock) {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "FainTv.db")
                        .fallbackToDestructiveMigration()
                        .build();
            }
            return INSTANCE;
        }
    }

    public static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE Repo "
                    + "ADD COLUMN html_url TEXT");
        }
    };
}
