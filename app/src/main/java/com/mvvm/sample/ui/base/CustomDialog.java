package com.mvvm.sample.ui.base;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mvvm.sample.R;

public class CustomDialog extends Dialog {
    private TextView mTitle;
    private TextView mMsg;
    private Button negativeButton;
    private Button positiveButton;
    private ImageView divider;
    private LinearLayout llButton;

    public CustomDialog(@NonNull Context context) {
        super(context, R.style.OverlayDialog);
        this.setContentView(R.layout.dialog_custom);
        mTitle = findViewById(R.id.exit_option_title);
        mMsg = findViewById(R.id.exit_option_msg);
        negativeButton = findViewById(R.id.exit_option_no);
        positiveButton = findViewById(R.id.exit_option_yes);
        negativeButton.setVisibility(View.GONE);
        positiveButton.setVisibility(View.GONE);
        divider = findViewById(R.id.divider);
        llButton = findViewById(R.id.ll_button);
    }

    public CustomDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected CustomDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    public void setTitle(int titleId) {
        mTitle.setText(titleId);
    }

    @Override
    public void setTitle(@Nullable CharSequence title) {
        mTitle.setText(title);
    }

    public CustomDialog setMessage(int titleId) {
        mMsg.setText(titleId);
        return this;
    }

    public CustomDialog setMessage(@Nullable CharSequence title) {
        mMsg.setText(title);
        return this;
    }

    public CustomDialog showMsg(boolean isShow) {
        if (mMsg != null) {
            if (isShow) {
                mMsg.setVisibility(View.VISIBLE);
            } else {
                mMsg.setVisibility(View.INVISIBLE);
            }
        }
        return this;
    }

    public CustomDialog showTitle(boolean isShow) {
        if (mTitle != null) {
            if (isShow) {
                mTitle.setVisibility(View.VISIBLE);
            } else {
                mTitle.setVisibility(View.INVISIBLE);
            }
        }
        return this;
    }

    public CustomDialog setNegativeButton(View.OnClickListener listener) {
        setOnClickListener(negativeButton, listener);
        return this;
    }

    public CustomDialog setNegativeButton(int textId, View.OnClickListener listener) {
        setOnClickListener(negativeButton, listener);
        negativeButton.setText(textId);
        return this;
    }

    public CustomDialog setNegativeButton(CharSequence text, View.OnClickListener listener) {
        setOnClickListener(negativeButton, listener);
        negativeButton.setText(text);
        return this;
    }

    public CustomDialog setPositiveButton(View.OnClickListener listener) {
        setOnClickListener(positiveButton, listener);
        return this;
    }

    public CustomDialog setPositiveButton(int textId, View.OnClickListener listener) {
        setOnClickListener(positiveButton, listener);
        positiveButton.setText(textId);
        return this;
    }

    public CustomDialog setPositiveButton(CharSequence text, View.OnClickListener listener) {
        setOnClickListener(positiveButton, listener);
        positiveButton.setText(text);
        return this;
    }

    public void setOnClickListener(Button button, View.OnClickListener listener) {
        button.setVisibility(View.VISIBLE);
        if (listener == null) {
            button.setOnClickListener(view -> cancel());
        } else {
            button.setOnClickListener(listener);
        }
    }

    public void cancelDialogByKey() { // no button
        divider.setVisibility(View.GONE);
        llButton.setVisibility(View.GONE);
        setOnKeyListener((DialogInterface dialog, int keyCode, KeyEvent event) -> {
            cancel();
            return false;
        });
    }

}
