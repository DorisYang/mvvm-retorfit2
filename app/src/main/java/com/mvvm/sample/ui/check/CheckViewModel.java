package com.mvvm.sample.ui.check;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.mvvm.sample.api.ApiResponse;
import com.mvvm.sample.repository.UserRepository;
import com.mvvm.sample.vo.BoxInfo;
import com.mvvm.sample.vo.ErrorCode;
import com.mvvm.sample.vo.Resource;

import javax.inject.Inject;

import okhttp3.ResponseBody;

public class CheckViewModel extends ViewModel {
    UserRepository mUserRepository;

    @Inject
    public CheckViewModel(UserRepository userRepository) {
        mUserRepository = userRepository;
    }

    public LiveData<Resource<BoxInfo>> checkMacAddress() {
        return mUserRepository.checkMacAddress("sdfsdfsdfds");
    }

    public LiveData<Boolean> isShowLoadingScreen() {
        return mUserRepository.getShowStatus();
    }

    public MutableLiveData<ErrorCode> occurErrorMesage(){
        return mUserRepository.getErrorMesage();
    }

    public LiveData<ApiResponse<ResponseBody>> connectToGoogle(){
        return mUserRepository.connectToGoogle();
    }
}
