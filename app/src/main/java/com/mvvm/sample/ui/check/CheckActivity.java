package com.mvvm.sample.ui.check;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.mvvm.sample.R;
import com.mvvm.sample.ui.base.BaseActivity;
import com.mvvm.sample.util.MyLog;
import com.mvvm.sample.vo.BoxInfo;

import java.util.Locale;

import javax.inject.Inject;

public class CheckActivity extends BaseActivity {

    @Inject
    CheckViewModel checkViewModel;
    int i = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check);
        checkViewModel.checkMacAddress().observe(this, data -> {
            switch (data.status) {
                case ERROR:
                    httpErrorMsg(data.errorCode, data.message);
                    break;
                case LOADING:
                    break;
                case SUCCESS:
                    MyLog.debug("hello:" + data.data.isBound);
                    break;
            }
        });

        checkViewModel.isShowLoadingScreen().observe(this, isShow -> {
            if (isShow) {
                showLoadingDialog();
            } else {
                closeLoadingDialog();
            }
        });

        checkViewModel.connectToGoogle().observe(this, responseBodyApiResponse -> {
            MyLog.debug(responseBodyApiResponse.code);
        });


    }

}
