package com.mvvm.sample.ui.base;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.mvvm.sample.R;
import com.mvvm.sample.util.DialogFactory;
import com.mvvm.sample.util.MyLog;

import dagger.android.AndroidInjection;

public class BaseActivity extends AppCompatActivity implements IBaseView {
    private Dialog dialog;
    private CustomDialog customDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void showLoadingDialog() {
        if (dialog == null)
            dialog = DialogFactory.createLoadingDialog(this);
        dialog.show();
    }

    @Override
    public void closeLoadingDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public void showInfoDialog(String info) {

    }

    @Override
    public void httpErrorMsg(String errorCode, String msg) {
        if (customDialog == null)
            customDialog = DialogFactory.createCustomDialog(this);
        customDialog.setMessage(msg).setPositiveButton(null).setTitle(errorCode);
        customDialog.create();
        customDialog.show();
    }
}

