package com.mvvm.sample.ui.base;

public interface IBaseView {
    void showLoadingDialog();

    void closeLoadingDialog();

    void showInfoDialog(String info);

    void httpErrorMsg(String errorCode, String msg);
}
