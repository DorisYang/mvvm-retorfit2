package com.mvvm.sample.vo;

import android.arch.persistence.room.Ignore;

public class HttpStatus {
    public Status status;
    public ErrorCode errorCode;

    public class Status {
        public String message;
        public int code;
    }
}
