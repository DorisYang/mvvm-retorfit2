package com.mvvm.sample.vo.response;

import com.mvvm.sample.vo.BoxInfo;
import com.mvvm.sample.vo.HttpStatus;

public class CheckMacAddressResponse extends HttpStatus {
    public BoxInfo data;
}
