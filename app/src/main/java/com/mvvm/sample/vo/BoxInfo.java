package com.mvvm.sample.vo;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "BoxInfo")
public class BoxInfo {
    @PrimaryKey
    public int id = 1;

    @ColumnInfo(name = "isBound")
    public boolean isBound = false;

//    private Box() {
//        id = 1;
//    }
}
