package com.mvvm.sample.api;

import android.text.TextUtils;

import com.mvvm.sample.util.LiveDataCallAdapterFactory;

import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitGenerator {
    public static final String API_BASE_URL = "http://localhost//api/";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder builder;
    private static Retrofit retrofit;

    public static Retrofit getRetrofit() {
        if (retrofit == null) {
            synchronized (RetrofitGenerator.class) {
                if (retrofit == null) {
                    retrofit = getBuilder().client(httpClient.build()).build();
                }
            }
        }
        return retrofit;
    }

    public static Retrofit.Builder getBuilder() {
        if (builder == null) {
            builder = new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(new LiveDataCallAdapterFactory());

        }
        return builder;

    }

    public static <S> S createService(Class<S> serviceClass) {
        return createService(serviceClass, null, null);
    }

    public static <S> S createService(
            Class<S> serviceClass, String username, String password) {
        if (!TextUtils.isEmpty(username)
                && !TextUtils.isEmpty(password)) {
            String authToken = Credentials.basic(username, password);
            return createService(serviceClass, authToken);
        }

        return createService(serviceClass, null);
    }

    public static <S> S createService(
            Class<S> serviceClass, final String authToken) {
        if (!TextUtils.isEmpty(authToken)) {
            AuthenticationInterceptor interceptor =
                    new AuthenticationInterceptor(authToken);

            if (!httpClient.interceptors().contains(interceptor)) {
                httpClient.addInterceptor(interceptor);
            }
        }
        AcceptLanguageHeaderInterceptor languageHeaderInterceptor = new AcceptLanguageHeaderInterceptor();
        if (!httpClient.interceptors().contains(languageHeaderInterceptor)) {
            httpClient.addInterceptor(languageHeaderInterceptor);
        }
        return getRetrofit().create(serviceClass);
    }
}