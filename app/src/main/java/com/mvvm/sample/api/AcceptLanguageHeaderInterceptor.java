package com.mvvm.sample.api;

import android.os.Build;
import android.os.LocaleList;

import com.mvvm.sample.util.MyLog;

import java.io.IOException;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AcceptLanguageHeaderInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();
        Request requestWithHeaders = originalRequest.newBuilder()
                .header("Accept-Language", getLanguage())
                .build();
        return chain.proceed(requestWithHeaders);
    }

    private String getLanguage() {
        MyLog.debug(Locale.getDefault().toLanguageTag());
        return Locale.getDefault().toLanguageTag();
    }
}
