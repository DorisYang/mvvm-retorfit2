package com.mvvm.sample.api;

import android.arch.lifecycle.LiveData;

import com.mvvm.sample.vo.response.CheckMacAddressResponse;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface UserService {

    @POST("1.0/checkMACaddress")
    @FormUrlEncoded
    LiveData<ApiResponse<CheckMacAddressResponse>> checkMACAddress(@FieldMap Map<String, String> macAddress);

    @GET("https://www.google.com/")
    LiveData<ApiResponse<ResponseBody>> connectToGoogle();

}
